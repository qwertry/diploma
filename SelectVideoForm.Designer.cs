using System.ComponentModel;
using System.Windows.Forms;

namespace Diploma
{
    partial class SelectVideoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        /// 
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// private CheckBox Binarise;
       

        private void InitializeComponent()
        {
            this.BrowseVideoButton = new System.Windows.Forms.Button();
            this.ExecuteFramesButton = new System.Windows.Forms.Button();
            this.Binarise = new System.Windows.Forms.CheckBox();
            this.ConvertIntoMatrix = new System.Windows.Forms.CheckBox();
            this.Equalize = new System.Windows.Forms.CheckBox();
            this.Interpolate = new System.Windows.Forms.CheckBox();
            this.SaveGreyScale8Bit = new System.Windows.Forms.CheckBox();
            this.StartTime = new System.Windows.Forms.TextBox();
            this.EndTime = new System.Windows.Forms.TextBox();
            this.StartTimeLabel = new System.Windows.Forms.Label();
            this.EndTimeLabel = new System.Windows.Forms.Label();
            this.ProgressBar = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // BrowseVideoButton
            // 
            this.BrowseVideoButton.Location = new System.Drawing.Point(603, 11);
            this.BrowseVideoButton.Margin = new System.Windows.Forms.Padding(2);
            this.BrowseVideoButton.Name = "BrowseVideoButton";
            this.BrowseVideoButton.Size = new System.Drawing.Size(97, 28);
            this.BrowseVideoButton.TabIndex = 2;
            this.BrowseVideoButton.Text = "Browse";
            this.BrowseVideoButton.UseVisualStyleBackColor = true;
            this.BrowseVideoButton.Click += new System.EventHandler(this.BrowseVideoButton_Click);
            // 
            // ExecuteFramesButton
            // 
            this.ExecuteFramesButton.Enabled = false;
            this.ExecuteFramesButton.Location = new System.Drawing.Point(526, 292);
            this.ExecuteFramesButton.Name = "ExecuteFramesButton";
            this.ExecuteFramesButton.Size = new System.Drawing.Size(174, 49);
            this.ExecuteFramesButton.TabIndex = 0;
            this.ExecuteFramesButton.Text = "Start";
            this.ExecuteFramesButton.Click += new System.EventHandler(this.ExecuteFramesButton_Click);
            // 
            // Binarise
            // 
            this.Binarise.Location = new System.Drawing.Point(408, 15);
            this.Binarise.Name = "Binarise";
            this.Binarise.Size = new System.Drawing.Size(104, 24);
            this.Binarise.TabIndex = 3;
            this.Binarise.Text = "Binarise";
            this.Binarise.CheckedChanged += new System.EventHandler(this.Binarise_CheckedChanged);
            // 
            // ConvertIntoMatrix
            // 
            this.ConvertIntoMatrix.Location = new System.Drawing.Point(186, 15);
            this.ConvertIntoMatrix.Name = "ConvertIntoMatrix";
            this.ConvertIntoMatrix.Size = new System.Drawing.Size(184, 24);
            this.ConvertIntoMatrix.TabIndex = 4;
            this.ConvertIntoMatrix.Text = "Save pixels in files as matrices";
            this.ConvertIntoMatrix.CheckedChanged += new System.EventHandler(this.ConvertIntoMatrix_CheckedChanged);
            // 
            // Equalize
            // 
            this.Equalize.Location = new System.Drawing.Point(29, 81);
            this.Equalize.Name = "Equalize";
            this.Equalize.Size = new System.Drawing.Size(127, 24);
            this.Equalize.TabIndex = 5;
            this.Equalize.Text = "Equalize frames";
            this.Equalize.CheckedChanged += new System.EventHandler(this.Equalize_CheckedChanged);
            // 
            // Interpolate
            // 
            this.Interpolate.Location = new System.Drawing.Point(29, 15);
            this.Interpolate.Name = "Interpolate";
            this.Interpolate.Size = new System.Drawing.Size(104, 24);
            this.Interpolate.TabIndex = 6;
            this.Interpolate.Text = "Interpolate pixels";
            this.Interpolate.CheckedChanged += new System.EventHandler(this.Interpolate_CheckedChanged);
            // 
            // SaveGreyScale8Bit
            // 
            this.SaveGreyScale8Bit.Location = new System.Drawing.Point(186, 68);
            this.SaveGreyScale8Bit.Name = "SaveGreyScale8Bit";
            this.SaveGreyScale8Bit.Size = new System.Drawing.Size(186, 51);
            this.SaveGreyScale8Bit.TabIndex = 7;
            this.SaveGreyScale8Bit.Text = "Save greyscale images";
            this.SaveGreyScale8Bit.CheckedChanged += new System.EventHandler(this.SaveGreyScale8Bit_CheckedChanged);
            // 
            // StartTime
            // 
            this.StartTime.Location = new System.Drawing.Point(33, 307);
            this.StartTime.Name = "StartTime";
            this.StartTime.Size = new System.Drawing.Size(100, 20);
            this.StartTime.TabIndex = 8;
            // 
            // EndTime
            // 
            this.EndTime.Location = new System.Drawing.Point(309, 307);
            this.EndTime.Name = "EndTime";
            this.EndTime.Size = new System.Drawing.Size(100, 20);
            this.EndTime.TabIndex = 9;
            // 
            // StartTimeLabel
            // 
            this.StartTimeLabel.Location = new System.Drawing.Point(30, 270);
            this.StartTimeLabel.Name = "StartTimeLabel";
            this.StartTimeLabel.Size = new System.Drawing.Size(136, 23);
            this.StartTimeLabel.TabIndex = 10;
            this.StartTimeLabel.Text = "Start processing from (sec)";
            // 
            // EndTimeLabel
            // 
            this.EndTimeLabel.Location = new System.Drawing.Point(306, 270);
            this.EndTimeLabel.Name = "EndTimeLabel";
            this.EndTimeLabel.Size = new System.Drawing.Size(151, 23);
            this.EndTimeLabel.TabIndex = 11;
            this.EndTimeLabel.Text = "End processing from (sec)";
            // 
            // ProgressBar
            // 
            this.ProgressBar.Enabled = false;
            this.ProgressBar.Location = new System.Drawing.Point(12, 168);
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(700, 23);
            this.ProgressBar.TabIndex = 12;
            // 
            // SelectVideoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 353);
            this.Controls.Add(this.ExecuteFramesButton);
            this.Controls.Add(this.BrowseVideoButton);
            this.Controls.Add(this.Binarise);
            this.Controls.Add(this.ConvertIntoMatrix);
            this.Controls.Add(this.Equalize);
            this.Controls.Add(this.Interpolate);
            this.Controls.Add(this.SaveGreyScale8Bit);
            this.Controls.Add(this.StartTime);
            this.Controls.Add(this.EndTime);
            this.Controls.Add(this.StartTimeLabel);
            this.Controls.Add(this.EndTimeLabel);
            this.Controls.Add(this.ProgressBar);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "SelectVideoForm";
            this.Text = "SelectVideoForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Button BrowseVideoButton;
        private Button ExecuteFramesButton;

        private CheckBox Binarise;
        private CheckBox ConvertIntoMatrix;
        private CheckBox Equalize;
        private CheckBox Interpolate;
        private CheckBox SaveGreyScale8Bit;

        private TextBox StartTime;
        private TextBox EndTime;

        private Label StartTimeLabel;
        private Label EndTimeLabel;

        private ProgressBar ProgressBar;
    }
}

