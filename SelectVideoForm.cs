using MediaToolkit;
using MediaToolkit.Model;
using MediaToolkit.Options;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// This is the code for your desktop app.
// Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.

namespace Diploma
{

    public partial class SelectVideoForm : Form
    {

        
        public SelectVideoForm()
        {
            InitializeComponent();
        }

        private string mp4FilePath;
        private readonly string outputPath = @"C:\output";

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Click on the link below to continue learning how to build a desktop app using WinForms!
            System.Diagnostics.Process.Start("http://aka.ms/dotnet-get-started-desktop");

        }

        private async void BrowseVideoButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog choofdlog = new OpenFileDialog();
            choofdlog.Filter = "All Files (*.*)|*.*";
            choofdlog.FilterIndex = 1;
            choofdlog.Multiselect = false;

            if (choofdlog.ShowDialog() == DialogResult.OK)
            {
                mp4FilePath = choofdlog.FileName;
                //string[] arrAllFiles = choofdlog.FileNames; //used when Multiselect = true 
                this.ExecuteFramesButton.Enabled = true;
            }
        }

        private async void ExecuteFramesButton_Click(object sender, EventArgs e)
        {
            this.ProgressBar.Enabled = true;
            this.ProgressBar.Maximum = 100;
            this.ProgressBar.Step = 1;
            var task = Task.Run(async ()=> await ExecuteFrames());
        }

    private async Task ExecuteFrames()
        {
            using (var engine = new Engine())
            {
                var mp4 = new MediaFile { Filename = mp4FilePath };

                engine.GetMetadata(mp4);
                int start = 30;
                int end = 40;
                float fpsInterval = (float)1000 / (float)mp4.Metadata.VideoData.Fps;

                var i = start * fpsInterval;
                 Invoke(new Action(() =>
                    {
                        this.ProgressBar.Maximum = (int)(end * fpsInterval - start * fpsInterval);
                    }));
                while (i < (mp4.Metadata.Duration.Seconds + 60 * mp4.Metadata.Duration.Minutes) * mp4.Metadata.VideoData.Fps && i < end* fpsInterval)
                {

                    var options = new ConversionOptions
                    {
                        Seek = TimeSpan.FromMilliseconds(i * fpsInterval),
                        VideoFps = (int)mp4.Metadata.VideoData.Fps,
                    };
                    string imageInfo = string.Format("{0}\\image-{1}-", outputPath, i, "O.bmp");
                    var outputFile = new MediaFile() { Filename = imageInfo + "O.bmp" };

                    engine.GetThumbnail(mp4, outputFile, options);

                    GetBitMap(imageInfo);
                    i++;

                    Invoke(new Action(() =>
                    {
                        this.ProgressBar.Value += 1;
                    }));
                }
               
            }
        }

        private void GetBitMap(string fileLink)
        {
            
            Bitmap bmp = (Bitmap)Image.FromFile(fileLink+ "O.bmp");

            #region
            ////get image dimension
            //int width = bmp.Width;
            //int height = bmp.Height;

            ////3 bitmap for red green blue image
            //Bitmap rbmp = new Bitmap(bmp);
            //Bitmap gbmp = new Bitmap(bmp);
            //Bitmap bbmp = new Bitmap(bmp);

            ////red green blue image
            //for (int y = 0; y < height; y++)
            //{
            //    for (int x = 0; x < width; x++)
            //    {
            //        //get pixel value
            //        Color p = bmp.GetPixel(x, y);

            //        //extract ARGB value from p
            //        int a = p.A;
            //        int r = p.R;
            //        int g = p.G;
            //        int b = p.B;

            //        //set red image pixel
            //        rbmp.SetPixel(x, y, Color.FromArgb(a, r, 0, 0));


            //        gbmp.SetPixel(x, y, Color.FromArgb(a, 0, g, 0));

            //        //set blue image pixel
            //        bbmp.SetPixel(x, y, Color.FromArgb(a, 0, 0, b));

            //    }
            //}

            ////write (save) red image
            //rbmp.Save(fileLink + "R.bmp");

            ////write(save) green image
            //gbmp.Save(fileLink + "G.bmp");

            ////write (save) blue image
            //bbmp.Save(fileLink + "B.bmp");


            //clone.Save(fileLink + "test.bmp");
            #endregion

            
            if (this.SaveGreyScale8Bit.Checked)
            {
                Bitmap d = new Bitmap(bmp.Width, bmp.Height);

                for (int i = 0; i < bmp.Width; i++)
                {
                    for (int x = 0; x < bmp.Height; x++)
                    {
                        Color oc = bmp.GetPixel(i, x);
                        int grayScale = (int)((oc.R * 0.3) + (oc.G * 0.59) + (oc.B * 0.11));
                        Color nc = Color.FromArgb(oc.A, grayScale, grayScale, grayScale);
                        d.SetPixel(i, x, nc);
                    }
                }
                Bitmap clone = d.Clone(new Rectangle(0, 0, d.Width, d.Height), PixelFormat.Format8bppIndexed);
                clone.Save(fileLink + "8bit-greyscale.bmp");
            }
            #region
            // Image m = ToolStripRenderer.CreateDisabledImage(clone);
            // m.Save(fileLink + "greyscaleIMAGECLASS.bmp");
            #endregion
        }

        private void Interpolate_CheckedChanged(object sender, EventArgs e)
        {
          
        }

        private void SaveGreyScale8Bit_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void Binarise_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void Equalize_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void ConvertIntoMatrix_CheckedChanged(object sender, EventArgs e)
        {
            
        }
    }
}
